// Copyright (c) 2017 The Robigalia Project Developers Licensed under the Apache License, Version
// 2.0 <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option. All files in the project
// carrying such notice may not be copied, modified, or distributed except according to those
// terms.

#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(feature = "std")]
extern crate core;

extern crate serde;

use core::intrinsics::transmute;

use serde::de::{DeserializeSeed, IntoDeserializer, Visitor};
use serde::{Deserialize, Serialize};

use core::fmt::Display;

pub trait ByteSink {
    fn put_bytes(&mut self, buf: &[u8]) -> Result<(), ()>;

    #[inline]
    fn put_byte(&mut self, byte: u8) -> Result<(), ()> {
        self.put_bytes(&[byte])
    }
}

pub struct SliceSink<'a> {
    slice: &'a mut [u8],
    idx: usize,
}
impl<'a> SliceSink<'a> {
    #[inline]
    pub fn new(slice: &mut [u8]) -> SliceSink {
        SliceSink {
            slice,
            idx: 0,
        }
    }

    #[inline]
    pub fn length(&self) -> usize {
        self.idx
    }
}
impl<'a> ByteSink for SliceSink<'a> {
    #[inline]
    fn put_bytes(&mut self, buf: &[u8]) -> Result<(), ()> {
        let cnt = buf.iter().zip(self.slice[self.idx..].iter_mut())
            .map(|(src, dst)| *dst = *src)
            .count();
        if cnt == buf.len() {
            self.idx += cnt;
            Ok(())
        } else {
            Err(())
        }
    }
}

#[inline]
#[cold]
fn ns<T>() -> Result<T, Error> {
    if cfg!(debug_assertions) {
        panic!("not supported")
    }
    Err(Error::NotSupported)
}

#[derive(Debug)]
pub enum Error {
    EndOfStream,
    InvalidRepresentation,
    MoreElements,
    TooManyVariants,
    NotSupported,
    ApplicationError(&'static str),
    #[cfg(not(feature = "std"))]
    Custom,
    #[cfg(feature = "std")]
    Custom(String),
}

impl core::fmt::Display for Error {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        #[cfg(feature = "std")]
        use std::error::Error;

        match self {
            &::Error::ApplicationError(s) => write!(f, "application error: {}", s),
            _ => f.write_str(self.description()),
        }
    }
}

#[cfg(not(feature = "std"))]
impl serde::de::Error for Error {
    fn custom<T: Display>(_msg: T) -> Error {
        Error::Custom
    }
}

#[cfg(not(feature = "std"))]
impl serde::ser::Error for Error {
    fn custom<T: Display>(_msg: T) -> Error {
        Error::Custom
    }
}

#[cfg(feature = "std")]
impl serde::de::Error for Error {
    fn custom<T: Display>(msg: T) -> Error {
        Error::Custom(format!("{}", msg))
    }
}

#[cfg(feature = "std")]
impl serde::ser::Error for Error {
    fn custom<T: Display>(msg: T) -> Error {
        Error::Custom(format!("{}", msg))
    }
}

#[cfg(not(feature = "std"))]
impl Error {
    fn description(&self) -> &str {
        match self {
            &Error::EndOfStream => "end of stream reached but more data was needed",
            &Error::InvalidRepresentation => "invalid representation for a value",
            &Error::MoreElements => "there are more elements of the sequence remaining",
            &Error::TooManyVariants => "too many variants, only up to 256 are supported",
            &Error::NotSupported => "feature not supported",
            &Error::ApplicationError(s) => s,
            &Error::Custom => "some custom error that couldn't be reported",
        }
    }
}

#[cfg(feature = "std")]
impl std::error::Error for Error {
    fn description(&self) -> &str {
        match self {
            &Error::EndOfStream => "end of stream reached but more data was needed",
            &Error::InvalidRepresentation => "invalid representation for a value",
            &Error::MoreElements => "there are more elements of the sequence remaining",
            &Error::TooManyVariants => "too many variants, only up to 256 are supported",
            &Error::NotSupported => "feature not supported",
            &Error::ApplicationError(s) => s,
            &Error::Custom(ref s) => &s,
        }
    }
}

type SerializeResult<T> = Result<T, Error>;

/// Serialize a value into a ByteSink.
pub fn serialize_into<S, T>(sink: &mut S, val: &T) -> SerializeResult<()>
where
    S: ByteSink,
    T: Serialize
{
    T::serialize(val, &mut Serializer(sink))
}

/// Serialize a value into a buffer. Returns the number of bytes used.
pub fn serialize<T>(buf: &mut [u8], val: &T) -> SerializeResult<usize>
where
    T: Serialize
{
    let mut sink = SliceSink::new(buf);
    serialize_into(&mut sink, val)?;
    Ok(sink.length())
}

/// Deserialize a value from a buffer. Returns the number of bytes used.
pub fn deserialize<'a, T>(buf: &'a [u8]) -> SerializeResult<(T, usize)>
where
    T: Deserialize<'a>
{
    let mut deserializer = Deserializer { buf: buf, idx: 0 };
    let val = T::deserialize(&mut deserializer)?;
    Ok((val, deserializer.idx))
}

struct Serializer<'a, S: ByteSink>(&'a mut S);

macro_rules! make_writers {
    () => {};
    ($type:ty => $name:ident, $($tail:tt)*) => {
        #[inline]
        fn $name(&mut self, val: $type) -> Result<(), Error> {
            self.0.put_bytes(&val.to_be_bytes())
                .map_err(|_| Error::EndOfStream)
        }
        make_writers!($($tail)*);
    };
}

impl<'a, S: ByteSink> Serializer<'a, S> {
    make_writers! {
        u8 => write_u8,
        u16 => write_u16,
        u32 => write_u32,
        u64 => write_u64,
    }

    #[inline]
    fn write_size(&mut self, val: usize) -> Result<(), Error> {
        if val <= u16::max_value() as usize {
            self.write_u16(val as u16)
        } else {
            ns()
        }
    }

    fn write_bytes(&mut self, val: &[u8]) -> Result<(), Error> {
        self.write_size(val.len())
            .and_then(|_| self.0.put_bytes(val)
            .map_err(|_| Error::EndOfStream))
    }
}

impl<'b, 'a: 'b, S: ByteSink> serde::Serializer for &'b mut Serializer<'a, S> {
    type Ok = ();
    type Error = Error;
    type SerializeSeq = Self;
    type SerializeTuple = Self;
    type SerializeTupleStruct = Self;
    type SerializeTupleVariant = Self;
    type SerializeMap = serde::ser::Impossible<(), Error>;
    type SerializeStruct = Self;
    type SerializeStructVariant = Self;

    fn serialize_bool(self, v: bool) -> SerializeResult<()> {
        self.write_u8(if v { 1 } else { 0 })
    }

    fn serialize_u8(self, v: u8) -> SerializeResult<()> {
        self.write_u8(v)
    }

    fn serialize_u16(self, v: u16) -> SerializeResult<()> {
        self.write_u16(v)
    }

    fn serialize_u32(self, v: u32) -> SerializeResult<()> {
        self.write_u32(v)
    }

    fn serialize_u64(self, v: u64) -> SerializeResult<()> {
        self.write_u64(v)
    }

    fn serialize_i8(self, v: i8) -> SerializeResult<()> {
        self.write_u8(v as u8)
    }

    fn serialize_i16(self, v: i16) -> SerializeResult<()> {
        self.write_u16(v as u16)
    }

    fn serialize_i32(self, v: i32) -> SerializeResult<()> {
        self.write_u32(v as u32)
    }

    fn serialize_i64(self, v: i64) -> SerializeResult<()> {
        self.write_u64(v as u64)
    }

    fn serialize_f32(self, v: f32) -> SerializeResult<()> {
        self.write_u32(v.to_bits())
    }

    fn serialize_f64(self, v: f64) -> SerializeResult<()> {
        self.write_u64(v.to_bits())
    }

    fn serialize_str(self, v: &str) -> SerializeResult<()> {
        self.write_bytes(v.as_bytes())
    }

    fn serialize_char(self, c: char) -> SerializeResult<()> {
        let mut buf = [0u8; 4];
        let slice = c.encode_utf8(&mut buf).as_bytes();
        self.0.put_bytes(slice)
            .map_err(|_| Error::EndOfStream)
    }

    fn serialize_bytes(self, v: &[u8]) -> SerializeResult<()> {
        self.write_bytes(v)
    }

    fn serialize_none(self) -> SerializeResult<()> {
        self.write_u8(0)
    }

    fn serialize_some<T: Serialize + ?Sized>(self, v: &T) -> SerializeResult<()> {
        self.write_u8(1)?;
        v.serialize(self)
    }

    fn serialize_unit(self) -> SerializeResult<()> {
        Ok(())
    }

    fn serialize_unit_struct(self, _: &'static str) -> SerializeResult<()> {
        Ok(())
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
    ) -> SerializeResult<()> {
        if variant_index > 255 {
            debug_assert!(false, "too many enum variants: {}", _name);
            return Err(Error::TooManyVariants);
        }
        self.write_u8(variant_index as u8)
    }

    fn serialize_newtype_struct<T: Serialize + ?Sized>(
        self,
        _name: &'static str,
        value: &T,
    ) -> SerializeResult<()> {
        value.serialize(self)
    }

    fn serialize_newtype_variant<T: Serialize + ?Sized>(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        value: &T,
    ) -> SerializeResult<()> {
        if variant_index > 255 {
            debug_assert!(false, "too many enum variants: {}", _name);
            return Err(Error::TooManyVariants);
        }
        self.write_u8(variant_index as u8)?;
        value.serialize(self)
    }

    fn serialize_seq(self, len: Option<usize>) -> SerializeResult<Self> {
        match len {
            None => ns(),
            Some(len) => {
                self.write_size(len)?;
                Ok(self)
            }
        }
    }

    fn serialize_tuple(self, _len: usize) -> SerializeResult<Self> {
        Ok(self)
    }

    fn serialize_tuple_struct(self, _name: &'static str, _len: usize) -> SerializeResult<Self> {
        Ok(self)
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> SerializeResult<Self> {
        if variant_index > 255 {
            debug_assert!(false, "too many enum variants: {}", _name);
            return Err(Error::TooManyVariants);
        }
        self.write_u8(variant_index as u8)?;
        Ok(self)
    }

    fn serialize_map(self, _len: Option<usize>) -> SerializeResult<Self::SerializeMap> {
        ns()
    }

    fn serialize_struct(self, _name: &'static str, _len: usize) -> SerializeResult<Self> {
        Ok(self)
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> SerializeResult<Self> {
        if variant_index > 255 {
            debug_assert!(false, "too many enum variants: {}", _name);
            return Err(Error::TooManyVariants);
        }
        self.write_u8(variant_index as u8)?;
        Ok(self)
    }

    #[cfg(not(feature = "std"))]
    fn collect_str<T: Display + ?Sized>(self, _value: &T) -> SerializeResult<()> {
        ns()
    }
}

impl<'b, 'a: 'b, S: ByteSink> serde::ser::SerializeSeq for &'b mut Serializer<'a, S> {
    type Ok = ();
    type Error = Error;

    fn serialize_element<V: Serialize + ?Sized>(&mut self, value: &V) -> SerializeResult<()> {
        value.serialize(&mut **self)
    }

    fn end(self) -> SerializeResult<()> {
        Ok(())
    }
}

impl<'b, 'a: 'b, S: ByteSink> serde::ser::SerializeTuple for &'b mut Serializer<'a, S> {
    type Ok = ();
    type Error = Error;

    fn serialize_element<V: Serialize + ?Sized>(&mut self, value: &V) -> SerializeResult<()> {
        value.serialize(&mut **self)
    }

    fn end(self) -> SerializeResult<()> {
        Ok(())
    }
}

impl<'b, 'a: 'b, S: ByteSink> serde::ser::SerializeTupleStruct for &'b mut Serializer<'a, S> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<V: Serialize + ?Sized>(&mut self, value: &V) -> Result<(), Error> {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<(), Error> {
        Ok(())
    }
}

impl<'b, 'a: 'b, S: ByteSink> serde::ser::SerializeTupleVariant for &'b mut Serializer<'a, S> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<V: Serialize + ?Sized>(&mut self, value: &V) -> Result<(), Error> {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<(), Error> {
        Ok(())
    }
}

impl<'b, 'a: 'b, S: ByteSink> serde::ser::SerializeStruct for &'b mut Serializer<'a, S> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<V: Serialize + ?Sized>(
        &mut self,
        _: &'static str,
        value: &V,
    ) -> Result<(), Error> {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<(), Error> {
        Ok(())
    }
}

impl<'b, 'a: 'b, S: ByteSink> serde::ser::SerializeStructVariant for &'b mut Serializer<'a, S> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<V: Serialize + ?Sized>(
        &mut self,
        _: &'static str,
        value: &V,
    ) -> Result<(), Error> {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<(), Error> {
        Ok(())
    }
}

struct Deserializer<'a> {
    buf: &'a [u8],
    idx: usize,
}

macro_rules! make_readers {
    () => {};
    ($type:ident => $name:ident, $($tail:tt)*) => {
        #[inline]
        fn $name(&mut self) -> Result<$type, Error> {
            let mut buf = [0u8; core::mem::size_of::<$type>()];
            self.get_bytes(&mut buf)
                .ok_or_else(|| Error::EndOfStream)?;
            Ok($type::from_be_bytes(buf))
        }
        make_readers!($($tail)*);
    };
}

impl<'a> Deserializer<'a> {
    #[inline]
    fn get_bytes<'s>(&mut self, buf: &'s mut [u8]) -> Option<&'s [u8]> {
        let sbuf = &self.buf[self.idx..];
        if sbuf.len() >= buf.len() {
            for (dst, src) in buf.iter_mut().zip(sbuf.iter()) {
                *dst = *src;
            }
            self.idx += buf.len();
            Some(buf)
        } else {
            None
        }
    }

    #[inline]
    fn get_byte(&mut self) -> Option<u8> {
        self.get_bytes(&mut [0u8]).map(|s| s[0])
    }

    make_readers! {
        u8 => read_u8,
        u16 => read_u16,
        u32 => read_u32,
        u64 => read_u64,
    }

    #[inline]
    fn read_size(&mut self) -> Result<usize, Error> {
        self.read_u16()
            .map(|s| s as usize)
    }

    fn read_bytes(&mut self) -> Result<&'a [u8], Error> {
        let len = self.read_size()?;
        if self.idx + len <= self.buf.len() {
            let begin = self.idx;
            self.idx += len;

            Ok(&self.buf[begin..self.idx])
        } else {
            Err(Error::EndOfStream)
        }
    }

    #[inline]
    fn read_str(&mut self) -> Result<&'a str, Error> {
        core::str::from_utf8(self.read_bytes()?)
            .map_err(|_| Error::InvalidRepresentation)
    }
}

struct SeqAccess<'a, 'b: 'a> {
    deserializer: &'a mut Deserializer<'b>,
    len: usize,
}

impl<'a, 'b: 'a> serde::de::SeqAccess<'b> for SeqAccess<'a, 'b> {
    type Error = Error;

    fn next_element_seed<V: DeserializeSeed<'b>>(
        &mut self,
        seed: V,
    ) -> Result<Option<V::Value>, Error> {
        if self.len > 0 {
            self.len -= 1;
            Ok(Some(DeserializeSeed::deserialize(
                seed,
                &mut *self.deserializer,
            )?))
        } else {
            Ok(None)
        }
    }

    fn size_hint(&self) -> Option<usize> {
        Some(self.len)
    }
}

type DeserializeResult<T> = Result<T, Error>;

impl<'b, 'de: 'b> serde::Deserializer<'de> for &'b mut Deserializer<'de> {
    type Error = Error;

    fn deserialize_any<V: Visitor<'de>>(self, _visitor: V) -> DeserializeResult<V::Value> {
        ns()
    }

    fn deserialize_bool<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        let value: u8 = Deserialize::deserialize(self)?;
        match value {
            0 => visitor.visit_bool(false),
            1 => visitor.visit_bool(true),
            _ => Err(Error::InvalidRepresentation),
        }
    }

    fn deserialize_u8<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_u8(self.read_u8()?)
    }

    fn deserialize_u16<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_u16(self.read_u16()?)
    }

    fn deserialize_u32<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_u32(self.read_u32()?)
    }

    fn deserialize_u64<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_u64(self.read_u64()?)
    }

    fn deserialize_i8<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_i8(self.read_u8()? as i8)
    }

    fn deserialize_i16<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_i16(self.read_u16()? as i16)
    }

    fn deserialize_i32<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_i32(self.read_u32()? as i32)
    }

    fn deserialize_i64<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_i64(self.read_u64()? as i64)
    }

    fn deserialize_f32<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_f32(unsafe { transmute(self.read_u32()?) })
    }
    fn deserialize_f64<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_f64(unsafe { transmute(self.read_u64()?) })
    }

    fn deserialize_char<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        let c = self.get_byte().ok_or(Error::EndOfStream)?;
        let mut buf = [c, 0, 0, 0];
        match c >> 3 {
            0b00000 ... 0b10111 => Ok(1),
            0b11000 ... 0b11011 => Ok(2),
            0b11100 ... 0b11101 => Ok(3),
            0b11110 ... 0b11110 => Ok(4),
            _ => Err(Error::InvalidRepresentation),
        }
            .and_then(|len| self.get_bytes(&mut buf[1..len]).ok_or(Error::EndOfStream))?;

        core::str::from_utf8(&buf)
            .map_err(|_| Error::InvalidRepresentation)
            .map(|s| s.chars().next().unwrap())
            .and_then(|c| visitor.visit_char(c))
    }

    fn deserialize_str<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_borrowed_str(self.read_str()?)
    }

    fn deserialize_string<V: Visitor<'de>>(self, _visitor: V) -> DeserializeResult<V::Value> {
        ns()
    }

    fn deserialize_bytes<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_borrowed_bytes(self.read_bytes()?)
    }

    fn deserialize_byte_buf<V: Visitor<'de>>(self, _visitor: V) -> DeserializeResult<V::Value> {
        ns()
    }

    fn deserialize_option<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        let value: u8 = Deserialize::deserialize(&mut *self)?;
        match value {
            0 => visitor.visit_none(),
            1 => visitor.visit_some(self),
            _ => Err(Error::InvalidRepresentation),
        }
    }

    fn deserialize_unit<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        visitor.visit_unit()
    }

    fn deserialize_unit_struct<V: Visitor<'de>>(
        self,
        _name: &'static str,
        visitor: V,
    ) -> DeserializeResult<V::Value> {
        visitor.visit_unit()
    }

    fn deserialize_newtype_struct<V: Visitor<'de>>(
        self,
        _name: &str,
        visitor: V,
    ) -> DeserializeResult<V::Value> {
        visitor.visit_newtype_struct(self)
    }

    fn deserialize_seq<V: Visitor<'de>>(self, visitor: V) -> DeserializeResult<V::Value> {
        let len = Deserialize::deserialize(&mut *self)?;

        visitor.visit_seq(SeqAccess {
            deserializer: self,
            len: len,
        })
    }

    fn deserialize_tuple<V: Visitor<'de>>(
        self,
        len: usize,
        visitor: V,
    ) -> DeserializeResult<V::Value> {
        visitor.visit_seq(SeqAccess {
            deserializer: self,
            len: len,
        })
    }

    fn deserialize_tuple_struct<V: Visitor<'de>>(
        self,
        _name: &'static str,
        len: usize,
        visitor: V,
    ) -> DeserializeResult<V::Value> {
        self.deserialize_tuple(len, visitor)
    }

    fn deserialize_map<V: Visitor<'de>>(self, _visitor: V) -> DeserializeResult<V::Value> {
        Err(Error::NotSupported)
    }

    fn deserialize_struct<V: Visitor<'de>>(
        self,
        _name: &str,
        fields: &'static [&'static str],
        visitor: V,
    ) -> DeserializeResult<V::Value> {
        self.deserialize_tuple(fields.len(), visitor)
    }

    fn deserialize_enum<V: Visitor<'de>>(
        self,
        _enum: &'static str,
        _variants: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Error> {
        visitor.visit_enum(self)
    }

    fn deserialize_identifier<V: Visitor<'de>>(self, _visitor: V) -> DeserializeResult<V::Value> {
        // not the panic because it seems noone cares about these?
        Err(Error::NotSupported)
    }

    fn deserialize_ignored_any<V: Visitor<'de>>(self, _visitor: V) -> DeserializeResult<V::Value> {
        ns()
    }
}

impl<'b, 'de: 'b> serde::de::VariantAccess<'de> for &'b mut Deserializer<'de> {
    type Error = Error;

    fn unit_variant(self) -> Result<(), Error> {
        Ok(())
    }

    fn newtype_variant_seed<V: DeserializeSeed<'de>>(self, seed: V) -> DeserializeResult<V::Value> {
        DeserializeSeed::deserialize(seed, self)
    }

    fn tuple_variant<V: Visitor<'de>>(self, len: usize, visitor: V) -> DeserializeResult<V::Value> {
        serde::de::Deserializer::deserialize_tuple(self, len, visitor)
    }

    fn struct_variant<V: Visitor<'de>>(
        self,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Error> {
        serde::de::Deserializer::deserialize_tuple(self, fields.len(), visitor)
    }
}

impl<'b, 'de: 'b> serde::de::EnumAccess<'de> for &'b mut Deserializer<'de> {
    type Error = Error;
    type Variant = Self;

    fn variant_seed<V: DeserializeSeed<'de>>(self, seed: V) -> DeserializeResult<(V::Value, Self)> {
        let x: u8 = Deserialize::deserialize(&mut *self)?;
        let v = DeserializeSeed::deserialize(seed, (x as u32).into_deserializer())?;
        Ok((v, self))
    }
}
